# Attribute to Attribute with BEGAN #

### What is this repository for? ###

[BEGAN](https://github.com/carpedm20/began-tensorflow)
[reference](http://www.gpucomputers.com/photo-editing-with-generative-adversarial-networks-part-2/)

### Calculate the Attribute Vector Z_att ###

* Theory

 In order to modify attributes, first need to find a z vector representing each attribute. 
So first use E to compute the z vector for each image in the dataset. 
Then calculate attribute vectors as follows:
 for example, to find the attribute vector for “young”, subtract the average z vector of all images that don’t have 
 the “young” attribute from the average z vector of all images that have it.
 Images in CelebA have 40 binary attributes.

* Calculate the z(encode) vector(2 x 64 numpy array)  

- average the entire images attribute      : average_total_z.txt
- average of images with female attributes : average_female_attr.txt
- average of images with male   attributes : average_male_attr.txt
- average of images with young  attributes : average_young_attr.txt
- average of images with old    attributes : average_old_attr.txt

calculate the z vectory for each attribute 

  female_z = average_total_z - average_male_attr

  male_z = average_total_z - average_female_attr

  young_z = average_total_z - average_old_attr

  old_z = average_total_z - average_young_attr

* Implementation

![alt tag](result.png)

### Change the face with its attribute ###

* 
* 
* 
